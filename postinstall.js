// package.json - postinstall.js
const fs = require('fs')
const { exec } = require('child_process')
const mkdirp = require('mkdirp')

const pkg = require('./package.json')

// const tmpPkg = { ...pkg }
// tmpPkg.name = `*${pkg.name}`
// fs.writeFileSync('./package.json', JSON.stringify(tmpPkg))

mkdirp.sync('./tmp-latest')

exec(`cd tmp-latest; npm i ${pkg.repository.url}#master`, (err, stdout, stderr) => {
  if (err) {
    console.log(err)
    throw new Error(`${pkg.name} postinstall script failed to install the latest from master branch`)
  }

  console.log(`stdout: ${stdout}`)
  console.log(`stderr: ${stderr}`)
  // fs.writeFileSync('./package.json', JSON.stringify(pkg))
})
